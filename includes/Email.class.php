<?php

class Email {
    public static function sendFinishedMail(Project $project, Language $language) {
        $jsonText = @file_get_contents("./files/config.json");
        $config = json_decode($jsonText, true);
        $receiver = @$config["notification-email"];

        $Text = "<html><body>";
        $Text .= "Hi,<br /><br />The translation of <b>" . $project->getName() . "</b> into ";
        $Text .= "<b>" . $language->getHumanReadableName() . "</b> was marked as finished.<br />";
        $Text .= "You can now import the changes into the app and upload an update.<br /><br />";
        $Text .= "ByteHamster translation system.<br />";
        $Text .= "This mail was sent automatically.";
        $Text .= "</body></html>";

        $Header = "MIME-Version: 1.0\n";
        $Header .= "Content-type: text/html; charset=utf-8\n";
        $Header .= "From: Translator <system@" . $_SERVER['SERVER_NAME'] . ">\n";
        mail($receiver, "Translation finished", $Text, $Header);
    }
}
