<?php

class Project {
    private $name;
    private $config;

    public function __construct(string $name = null) {
        $this->name = $name;
        $this->config = new ProjectConfig($this);
    }

    function exists() {
        return $this->name != null && file_exists($this->getPath());
    }

    function getPath() {
        return "./files/" . $this->name;
    }

    function getName() {
        return $this->name;
    }

    function getConfig() {
        return $this->config;
    }

    function getLanguages() {
        $langs = array();
        $handle = opendir($this->getPath());
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".."
                    && $file != "values" && is_dir($this->getPath() . "/" . $file)) {
                $code = str_replace("values-", "", $file);
                $langs[] = new Language($this, $code);
            }
        }
        closedir($handle);
        sort($langs);
        return $langs;
    }

    function hasLanguage(String $code) {
        foreach ($this->getLanguages() as $language) {
            if (strtolower($language->getCode()) === strtolower($code)) {
                return true;
            }
        }
        return false;
    }

    function getResources() {
        return $this->config->getResources();
    }

    function findResource($name) {
        foreach ($this->getResources() as $resource) {
            if ($resource->getName() == $name) {
                return $resource;
            }
        }
        return null;
    }

    function getTranslators() {
        $translators = array();
        foreach ($this->getLanguages() as $lang) {
            $translators[] = $lang->getHumanReadableName() . " by " . $lang->getTranslator();
        }
        sort($translators);
        return $translators;
    }

    static function getProjects() {
        $projects = array();
        $handle = opendir('./files/');
        while ($handle !== false && false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".." && is_dir("./files/" . $file) && is_file("./files/" . $file . "/config.json")) {
                $projects[] = new Project($file);
            }
        }
        closedir($handle);
        sort($projects);
        return $projects;
    }

    static function create(string $name) {
        if (!file_exists("./files")) {
            mkdir("./files");
        }
        if(!file_exists("./files/$name")) {
            mkdir("./files/$name");
            mkdir("./files/$name/values");
            $defaultConfig = file_get_contents("./templates/default-project-config.json");
            file_put_contents("./files/$name/config.json", $defaultConfig);
            $project = new Project($name);
            foreach ($project->getResources() as $resource) {
                $resource->putRawOriginal("");
            }
            return $project;
        } else {
            return null;
        }
    }
}
