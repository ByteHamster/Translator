<?php

class ArrayResource extends Resource {

    public function getType() {
        return Resource::TYPE_ARRAY;
    }
    
    public function save(Language $language, $id, $value) {
        $xmlContent = $this->getRaw($language);
        $xml = new SimpleXMLElement(removeBom($xmlContent));
        $arrayFound = false;
        $saved = false;
        foreach ($xml->{'string-array'} as $array) {
            $name = $array["name"];
            if (startsWith($id, "array--".$name)) {
                $arrayFound = true;
                $i = 0;
                foreach ($array->item as $item) {
                    if ($id === "array--".$name."--".$i) {
                        $item[0] = toXML($value);
                        $saved = true;
                    }
                    $i++;
                }
            }
        }
        if (!$arrayFound) {
            $array_name = preg_replace("'array--(.*)--(.*)'", "$1", $id);
            $new = $xml->addChild("string-array");
            $new->addAttribute("name", $array_name);
            $xmlContent = $this->getRawOriginal($language->getProject());
            $original = new SimpleXMLElement(removeBom($xmlContent));
            foreach ($original->{'string-array'} as $array) {
                if ($array["name"] == $array_name) {
                    $i = 0;
                    foreach ($array->item as $item) {
                        if ($id == "array--".$array_name."--".$i) {
                            $saved = true;
                            $new_line = $new->addChild("item", toXML($value));
                        } else {
                            $new_line = $new->addChild("item");
                        }
                        $i++;
                    }
                }
            }
        }
        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml->asXML());
        $this->putRaw($language, $dom->saveXML());
        return $saved;
    }

    public function bootstrap(Language $language) {
        $this->putRaw($language, "<?xml version=\"1.0\" encoding=\"utf-8\"?><resources></resources>");
        return true;
    }
}
