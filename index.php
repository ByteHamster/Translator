<?php
include("includes/utils.php");
session_start();

if (isset($_POST["password"])) {
    Session::login($_POST["password"]);
}

if(@$_GET["admin"] == "false") {
    Session::logout();
}

?><html>
    <?php include("templates/head.html"); ?>
    <body>
        <?php

            if (!is_dir("./files")) {
                mkdir("./files");
                $defaultConfig = file_get_contents("./templates/default-main-config.json");
                file_put_contents("./files/config.json", $defaultConfig);
            }

            $success = false;
            $project = new Project(@$_GET["project"]);
            if ($project->exists()) {
                $language = new Language($project, @$_GET["language"]);
                if ($language->exists()) {
                    if ($language->hasAccess(@$_GET["access"])) {
                        $success = true;
                        include("includes/user.php");
                    } else {
                        echo "<p class='error'>Invalid access token</p>";
                    }
                }
            }

            if (!$success) {
                if (Session::isLoggedIn()) {
                    include("includes/admin.php");
                } else {
                    include("templates/login.html");
                }
            }
        ?>
        <span id="footer">Translation tool by <a class="simple" target="_blank" href="https://www.bytehamster.com/">ByteHamster</a></span>
    </body>
</html>
