<?php

abstract class Resource {
    const TYPE_TEXT = 0;
    const TYPE_ARRAY = 1;
    const TYPE_STRING = 2;

    private $name;
    private $description;
    private $project;
    private $destinationPath;

    public function __construct(Project $project, string $name) {
        $this->name = $name;
        $this->project = $project;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription(string $description) {
        $this->description = $description;
    }

    public function getDestinationPath() {
        return $this->destinationPath;
    }

    public function setDestinationPath(?string $destinationPath) {
        $this->destinationPath = $destinationPath;
    }

    public function getRaw(Language $language) {
        return @file_get_contents($language->getPath() . "/" . $this->name);
    }

    public function getRawOriginal() {
        return @file_get_contents($this->project->getPath() . "/values/" . $this->name);
    }

    public function putRawOriginal(string $content) {
        file_put_contents_utf8($this->project->getPath()."/values/".$this->name, $content);
    }

    public function putRaw(Language $language, string $content) {
        file_put_contents_utf8($language->getPath()."/".$this->name, $content);
    }

    public abstract function save(Language $language, $id, $value);
    public abstract function bootstrap(Language $language);
    public abstract function getType();

    public function getName() {
        return $this->name;
    }
}
