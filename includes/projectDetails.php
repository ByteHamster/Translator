<?php
$project = new Project(@$_GET["project"]);

echo "<a href=\"./\" class=\"simple\">« Back to project list</a><br />";
if (@$_GET["do"] == "add_language") {
    echo "<h2>Add language</h2>";
    echo "<form class=\"simple\" method=\"POST\" action=\"./?project=".$_GET["project"]."\">";
    echo "<input maxlength=\"6\" spellcheck=\"false\" class=\"box\" id=\"language-code\" name=\"language-code\" value=\"DE\" />";
    echo "<input type=\"submit\" class=\"submit\" value=\"Add\">";
    echo "<a class=\"button secondary\" href=\"./?project=".$_GET["project"]."\">Cancel</a><br />";
    echo "<script>setTimeout(function() { document.getElementById('language-code').focus(); }, 10);</script>";
    echo "</form>";
} else if(@$_GET["do"] == "delete") {
    echo "<p class=\"warning\">Delete project ".$_GET["project"]."?<br /><br /><a class=\"button secondary inline\" href=\"./?project=".$_GET["project"]."&do=godelete\">Delete</a> <a class=\"button inline\" href=\"./?project=".$_GET["project"]."\">Cancel</a></p>";
} else if (isset($_GET["editLang"])) {
    $language = new Language($project, $_GET["editLang"]);
    if (!$language->exists()) {
        // Do nothing
    } else if (@$_GET["do"] == "goDeleteLanguage") {
        $language->delete();
        echo "<p class=\"success\">✔ Language was deleted.</p>";
    } else if (@$_GET["do"] == "setfinished") {
        $language->setFinished(filter_var($_GET['finished'], FILTER_VALIDATE_BOOLEAN));
    } else if(@$_GET["do"] == "deleteLanguage") {
        echo "<p class=\"warning\">Delete language ".$language->getHumanReadableName()." from project ".$project->getName()."?<br /><br /><a class=\"button secondary inline\" href=\"./?project=".$project->getName()."&editLang=".$_GET["editLang"]."&do=goDeleteLanguage\">Delete</a> <a class=\"button inline\" href=\"./?project=".$project->getName()."\">Cancel</a></p>";
    }
}

if (isset($_POST["language-code"])) {
    $lang = strtolower($_POST["language-code"]);
    if(!file_exists("./files/".$_GET["project"]."/values-$lang")) {
        @mkdir("./files/".$_GET["project"]."/values-$lang");
        $config=array("locked" => false, "password" => generateRandomString(20));
        file_put_contents("./files/".$_GET["project"]."/values-$lang/config.json", json_encode($config, JSON_PRETTY_PRINT));

        $newLanguage = new Language($project, $lang);
        foreach ($project->getResources() as $resource) {
            $resource->bootstrap($newLanguage);
        }
        echo "<p class=\"success\">✔ Language added.</p>";
    } else {
        echo "<p class=\"error\">✘ Language already exists.</p>";
    }
}

if (@$_GET["do"] == "godelete") {
    deleteDir("./files/" . $_GET["project"] . "/");
    echo "<p class=\"success\">✔ Project was deleted.</p>";
} else {
    echo "<h2>Project: ".$_GET["project"]."</h2><br />";
    echo "<a class=\"button inline\" href=\"./?project=".$_GET["project"]."&do=components\">Manage components</a>";
    if(!$project->exists()) {
        echo "<p class=\"error\">✘ Project does not exist.</p>";
    } else {
        echo "<a class=\"button inline\" href=\"./?project=".$_GET["project"]."&do=add_language\">Add language</a>";
        echo "<a class=\"button inline secondary\" target=\"_blank\" href=\"./downloader.php?project=".$_GET["project"]."\">Download resources</a>";
        echo "<a class=\"button inline secondary\" href=\"./?project=".$_GET["project"]."&do=delete\">Delete</a>";

        if (count($project->getResources()) == 0) {
            echo "<p class=\"warning\">Create a component to get started</p>";
        } else if (count($project->getLanguages()) == 0) {
            echo "<p class=\"warning\">Create a language to start translating</p>";
        }

        echo "<br /><h2>Translations</h2><br />";

        echo "<table>";
        foreach ($project->getLanguages() as $lang) {
            echo "<tr>";
            echo "<td>";
            if ($lang->isFinished()) {
                echo "<a class=\"button\" style=\"width: 50px;\" href=\"./?project=".$_GET["project"]."&editLang=".$lang->getCode()."&do=setfinished&finished=false\">✔</a>";
                echo "<span style=\"color:#999;font-size: 16px;\">Finished. Editing locked.</span><br />";
            } else {
                echo "<a class=\"button\" style=\"width: 50px;\" href=\"./?project=".$_GET["project"]."&editLang=".$lang->getCode()."&do=setfinished&finished=true\">⌛</a>";
                echo "<span style=\"color:#999;font-size: 16px;\">Invite your translator!</span><br />";
            }
            echo "</td>";
            echo "<td>" . $lang->getHumanReadableName() . " (" . $lang->getCode() . ")</td>";

            echo "<td>";
            echo "<a class=\"button\" target=\"_blank\" href=\"?project=".$_GET["project"]."&access=".$lang->getAccess()."&language=".$lang->getCode()."\">Translate</a>";
            echo "<a class=\"button secondary\" target=\"_blank\" href=\"./?project=".$_GET["project"]."&verify=".$lang->getCode()."\">Verify</a>";
            echo "<a class=\"button secondary\" href=\"./?project=".$_GET["project"]."&editLang=".$lang->getCode()."&do=deleteLanguage\">Delete</a>";
            echo "</td>";

            echo "</tr>";
        }

        echo "</table><br /><br /><div class=\"infobox\">";
        echo implode("<br />", $project->getTranslators());
        echo "</div>";
    }
}
