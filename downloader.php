<?php
include("includes/utils.php");

session_start();

if (!Session::isLoggedIn()) {
    header("HTTP/1.1 403 Forbidden");
    die("Permission error");
} else {
    $project = new Project($_GET["project"]);
    if (!$project->exists()) {
        header("HTTP/1.1 404 Not Found");
        die("Project not found: " . $project->getName());
    }
    
    $packager = new Packager($project);
    $packager->packAllLanguages();
    header("Content-Disposition: attachment; filename=\"" . $project->getName() . ".zip\"");
    header("Content-Type: application/zip");
    
    readfile($packager->getPath());
    $packager->recycle();
    exit;
}
