<?php

class TranslationTable {
    private $project;
    private $language;

    public function __construct(Project $project, Language $language) {
        $this->project = $project;
        $this->language = $language;
    }

    private static function linkifyFormatSpecifiers(string $str) {
        return preg_replace('/(?<=^|\W)(%(?:(?:\d\$)?[sdf]|\d))(?=$|\W)/', 
            '<span class="format_specifier" onclick="insertIntoEditor(this, \'$1\');"'
            .'title="This code is replaced with content by the application.">$1</span>', $str);
    }
    
    public function printHead() {
        echo "<table class=\"translate\">";
        echo "<tr class=\"heading\"><td>Original</td><td>Translation</td></tr>";
    }
    
    public function printTranslator() {
        echo "<tr><td><em>&lt;Your name for the credits section&gt;</em></td><td>".editor("txt--translator",$this->language->getTranslator(),"","translator")."</td></tr>";
    }
    
    public function printFooter() {
        echo "</table>";
    }
    
    public function translateFile(Resource $resource) {
        $original = trim(removeBom($resource->getRawOriginal($this->project)));
        if ($original != "") {
            echo "<tr><td>";
            echo self::linkifyFormatSpecifiers(toHTML($original));
            $translation = $resource->getRaw($this->language);
            echo "</td><td>".editor("txt--".$resource->getName(), $translation, $original,$resource->getName())."</td></tr>";
        }
    }

    public function translateArrays(ArrayResource $resource) {
        $array_source = trim(removeBom($resource->getRawOriginal($this->project)));
        $array_translate = trim(removeBom($resource->getRaw($this->language)));
        if ($array_translate != "" && $array_source != "") {
            $translated = array();
            $xml = new SimpleXMLElement($array_translate);
            foreach ($xml->{'string-array'} as $array) {
                $i = 0;
                foreach ($array->item as $item) {
                    $name = $array["name"];
                    $id = "array--".$name."--".$i;
                    $translated[$id] = $item;
                    $i++;
                }
            }
            $xml = new SimpleXMLElement($array_source);
            foreach ($xml->{'string-array'} as $array) {
                $i = 0;
                foreach ($array->item as $item) {
                    $name = $array["name"];
                    echo "<tr><td>";
                    echo self::linkifyFormatSpecifiers(toHTML($item));
                    $id = "array--".$name."--".$i;
                    echo "</td><td>".editor($id,@$translated[$id],$item,$resource->getName())."</td></tr>";
                    $i++;
                }
            }
        }
    }

    public function translateStrings(StringResource $resource) {
        $string_source = trim(removeBom($resource->getRawOriginal($this->project)));
        $string_translate = trim(removeBom($resource->getRaw($this->language)));
        if ($string_translate != "" && $string_source != "") {
            $translated = array();
            $xml = new SimpleXMLElement($string_translate);
            foreach ($xml->string as $string) {
                    $id = "string--".$string["name"];
                    $translated[$id] = $string;
            }
            $xml = new SimpleXMLElement($string_source);
            foreach ($xml->string as $string) {
                if($string['translatable'] != "false") {
                    echo "<tr><td>";
                    echo self::linkifyFormatSpecifiers(toHTML($string));
                    $id = "string--".$string["name"];
                    echo "</td><td>".editor($id,@$translated[$id],$string,$resource->getName())."</td></tr>";
                }
            }
        }
    }
}
