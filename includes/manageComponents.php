<?php

function typeToString($type) {
    switch ($type) {
        case Resource::TYPE_TEXT:
            return "Plain text";
        case Resource::TYPE_ARRAY:
            return "Android array resource";
        case Resource::TYPE_STRING:
            return "Android string resource";
    }
}

if (isset($_GET["component"])) {
    $resource = $project->findResource($_GET["component"]);

    echo "<a href=\"./?project=" . $_GET["project"] . "&do=components\" class=\"simple\">« Back to components of \"" . $_GET["project"] . "\"</a><br />";
    echo "<h2>Component: " . $resource->getDescription() . "</h2><br />";

    if (isset($_POST["upload-rawfile"])) {
        $resource->putRawOriginal(fromPlainHTML($_POST["upload-rawfile"]));
        $resource->setDescription($_POST["description"]);
        $project->getConfig()->updateResource($resource, "description", $_POST["description"]);
        $resource->setDestinationPath($_POST["destination"]);
        $project->getConfig()->updateResource($resource, "destinationPath", $_POST["destination"]);
        echo "<p class=\"success\">✔ Saved successfully.</p>";
    } else if (@$_GET["delete"] == "ask") {
        echo "<p class=\"warning\">Delete component " . $resource->getName() . "?<br /><br />";
        echo "<a href=\"./?project=" . $_GET["project"] . "&do=components&delete=go&component=".urlencode($resource->getName())."\" class=\"button secondary\">Delete</a>";
        echo "<a href=\"./?project=" . $_GET["project"] . "&do=components&component=".urlencode($resource->getName())."\" class=\"button\">Cancel</a>";
    }
    if (@$_GET["delete"] == "go") {
        $project->getConfig()->deleteResource($resource);
        echo "<p class=\"success\">✔ Resource deleted</p>";
    } else {
        echo "<form class=\"upload\" method=\"POST\" action=\"./?project=" . $_GET["project"] . "&do=components&component=" . urlencode($resource->getName()) . "\">";

        echo "<table>";
        echo "<tr><td>ID</td>";
        echo "<td>" . $resource->getName() . "</td></tr>";

        echo "<tr><td>Type</td>";
        echo "<td>" . typeToString($resource->getType()) . "</td></tr>";

        echo '<tr><td>Destination path<br/><span class="notice">Variables:</span> <span class="format_specifier" title="ISO language code ">$languageCode</span>, <span class="format_specifier" title="Google Play compatible language code">$playLanguage</span></td>';
        echo "<td><input style=\"width: 100%\" type=\"text\" class=\"box\" name=\"destination\" value=\"" . $resource->getDestinationPath() . "\"></td></tr>";

        echo "<tr><td>Description</td>";
        echo "<td><input style=\"width: 100%\" type=\"text\" class=\"box\" name=\"description\" value=\"" . str_replace("\"", "\\\"", $resource->getDescription()) . "\"></td></tr>";

        echo "<tr><td>Base file content</td>";
        echo "<td><textarea spellcheck=\"false\" name=\"upload-rawfile\" wrap=\"off\">" . toPlainHTML($resource->getRawOriginal($project)) . "</textarea></td></tr>";

        echo "</table><br /><input type=\"submit\" class=\"submit\" value=\"Save\">";
        echo "<a href=\"./?project=" . $_GET["project"] . "&do=components&delete=ask&component=".urlencode($resource->getName())."\" class=\"button secondary\">Delete</a>";
        echo "</form>";
    }
} else {
    echo "<a href=\"./?project=" . $_GET["project"] . "\" class=\"simple\">« Back to project \"" . $_GET["project"] . "\"</a><br />";
    echo "<h2>Components of: " . $_GET["project"] . "</h2><br />";

    if (isset($_POST["ID"])) {
        if ($project->findResource($_POST["ID"]) !== null) {
            echo "<p class=\"error\">✘ Component ID already exists</p>";
        } else {
            $project->getConfig()->createResource($_POST["ID"], $_POST["type"]);
            echo "<p class=\"success\">✔ Component created</p>";
        }
    }

    if (isset($_GET["create"])) {
        echo "<form class=\"simple\" method=\"POST\" action=\"./?project=" . $_GET["project"] . "&do=components\">";
        echo "<input spellcheck=\"false\" class=\"box\" name=\"ID\" value=\"ID\" /><br/>";
        echo "<select style=\"width: 100%\" name=\"type\" class=\"box\">";
        echo "<option value=\"text\">Plain text</option>";
        echo "<option value=\"array\">Android array resource</option>";
        echo "<option value=\"string\">Android string resource</option>";
        echo "</select><br/><br/>";
        echo "<input type=\"submit\" class=\"submit\" value=\"Create\">";
        echo "<a href=\"./?project=" . $_GET["project"] . "&do=components\" class=\"button secondary\">Cancel</a>";
        echo "</form>";
    } else {
        echo "<a href=\"./?project=" . $_GET["project"] . "&do=components&create=true\" class=\"button\" style=\"width:200px;\">Add component</a>";
    }

    echo "<table>";
    foreach ($project->getResources() as $resource) {
        echo "<tr><td>" . $resource->getDescription() . "</td>";
        echo "<td><a href=\"./?project=" . $_GET["project"] . "&do=components&component=".urlencode($resource->getName())."\" class=\"button\">Edit</a></td></tr>";
    }
    echo "</table>";
}
