<?php

class Language {
    private $code = null;
    private $project;
    private $config;
    
    public function __construct(Project $project, string $code = null) {
        $this->project = $project;
        if ($code != null) {
            $this->code = strtolower($code);
        }
        $jsonText = @file_get_contents($this->getPath() . "/config.json");
        $this->config = json_decode($jsonText, true);
    }
    
    function getPath() {
        return $this->project->getPath() . "/values-" . $this->code;
    }

    function exists() {
        return $this->code != null
            && $this->project->hasLanguage($this->code)
            && file_exists($this->getPath());
    }
    
    function isFinished() {
        return $this->config["locked"];
    }
    
    function setFinished(bool $state) {
        $this->config["locked"] = $state;
        file_put_contents($this->getPath() . "/config.json", json_encode($this->config, JSON_PRETTY_PRINT));
    }
    
    function getAccess() {
        return $this->config["password"];
    }

    function setAccess(string $password) {
        $this->config["password"] = $password;
        file_put_contents($this->getPath() . "/config.json", json_encode($this->config, JSON_PRETTY_PRINT));
    }

    function getProject() {
        return $this->project;
    }

    function hasAccess($password) {
        if (!$this->exists()) {
            return false;
        }
        return $password === $this->getAccess();
    }
    
    function delete() {
        deleteDir($this->getPath());
    }
    
    function getCode() {
        return $this->code;
    }
    
    function getTranslator() {
        $t = @file_get_contents($this->getPath() . "/translator");
        return trim(removeBom($t));
    }
    
    function getGooglePlayName() {
        # https://support.google.com/googleplay/android-developer/answer/3125566#available
        $language = $this->getCode();
        $trans = array(
            "ar" => "ar",
            "cs" => "cs-CZ",
            "de" => "de-DE",
            "es" => "es-ES",
            "fi" => "fi-FI",
            "fr" => "fr-FR",
            "hu" => "hu-HU",
            "id" => "id",
            "it" => "it-IT",
            "ja" => "ja-JP",
            "ms" => "ms",
            "nl" => "nl-NL",
            "pl" => "pl-PL",
            "pt" => "pt-BR",
            "ro" => "ro",
            "ru" => "ru-RU",
            "sv" => "sv-SE",
            "tr" => "tr-TR",
            "vi" => "vi",
            "zh" => "zh-CN",
        );
        return strtr($language, $trans);
    }

    function getHumanReadableName() {
        # php intl extension needed
        return Locale::getDisplayLanguage($this->getCode(), 'en');
    }
}
