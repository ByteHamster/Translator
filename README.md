# Translator

Allows to easily translate Android apps. The client application directly downloads the translations into your app's source tree.

**The tool currently is in beta stage**

![Screenshot](screenshot.png)

## Configuring a project

You can use the admin UI for configuring projects.

## Using the command line client

Create a `.translaterc` file in your home folder with the following content:

```
host=https://translate.example.com 
password=secret
```

Then create a `.translator` file in the root directory of your project to specify which project to download.

```
project=VolumeManager
```
