<?php

class Packager {
    private $zip;
    private $project;
    private $filename = "./tmp.zip";
    
    public function __construct($project) {
        $this->project = $project;
        $this->zip = new ZipArchive();

        if ($this->zip->open($this->filename, ZipArchive::CREATE) !== true) {
            header("HTTP/1.1 500 Internal Server Error");
            die("Can not open temp file " . $this->filename);
        }

        $translators = $project->getTranslators();
        $this->zip->addFromString("main/res/raw/translators.txt", implode("\n", $translators));
    }
    
    private function addNonEmpty($from, $to) {
        $string = file_get_contents($from);
        if (!contains($string, "<resources></resources>") && !stringEmpty($string)) {
            $this->zip->addFile($from, $to);
        }
    }

    private function addLanguage(Language $language) {
        $resfolder = $language->getPath();

        $resources = $this->project->getResources();
        foreach ($resources as $resource) {
            if ($resource->getDestinationPath() != NULL) {
                $destination = $this->replacePaths($resource->getDestinationPath(), $language);
                $this->addNonEmpty($resfolder."/".$resource->getName(), $destination);
            }
        }
    }

    private function replacePaths(string $path, Language $language) {
        $playLanguage = $language->getGooglePlayName();
        $languageCode = $language->getCode();
        return str_replace(['$playLanguage', '$languageCode'], [$playLanguage, $languageCode], $path);
    }
    
    function packLanguage(Language $language) {
        $this->addLanguage($language);
        $this->zip->close();
    }
    
    function packAllLanguages() {
        $langs = $this->project->getLanguages();
        foreach ($langs as $lang) {
            if ($lang->isFinished()) {
                $this->addLanguage($lang);
            }
        }
        $this->zip->close();
    }
    
    function getPath() {
        return $this->filename;
    }
    
    function recycle() {
        unlink($this->filename);
    }
}
